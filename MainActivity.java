package com.luxand.facerecognition.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.luxand.facerecognition.R;
import com.luxand.facerecognition.managers.MenuManager; /// <-- Code for work with menu items
import com.luxand.facerecognition.managers.PcasConnectionManager;
import com.luxand.facerecognition.managers.UserManager;
import com.luxand.facerecognition.models.User;
import com.luxand.facerecognition.views.PopupMessage;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.pcas_project.client.android.api.sa.AndroidNode;
import eu.pcas_project.client.android.ipc.PcasConnection;
import eu.pcas_project.client.shared.api.DeadDeviceException;
import eu.pcas_project.client.shared.api.DeadServiceException;
import eu.pcas_project.client.shared.api.Result;
import eu.pcas_project.client.shared.api.SecureApp;
import eu.pcas_project.common.util.Erco;

public class MainActivity extends Activity {

// Code is truncated in order to show only menu implementation  
  

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

 
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        MenuManager menuManager = new MenuManager();
        switch (item.getItemId()) {
            case R.id.menu_logout:
                menuManager.restartRegistrstionProcess(this);
                return true;
            case R.id.menu_erase_user:
			    userManager.eraseUser;
                menuManager.restartRegistrstionProcess(this);
                return true;
            case R.id.menu_help:
                menuManager.menuHelp(this);
                return true;
            case R.id.menu_exit:
                this.finishAffinity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

  
}


